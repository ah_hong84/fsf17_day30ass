var bcrypt = require('bcryptjs');
var config = require('./config');
var database = require('./database');

var User = database.User;

module.exports = function() {
    if(config.seed) {
        var hashpassword = bcrypt.hashSync("Password@123", bcrypt.genSaltSync(8), null);
        User
            .create({
                username: "hafiz",
                password: hashpassword,
                admin: true,
                iqdat: true,
                testtime: true
            })
            .then(function(user) {
                console.log(user);
            })
            .catch(function(err) {
                console.log(err);
            });
        
        hashpassword = bcrypt.hashSync("pun3ggol", bcrypt.genSaltSync(8), null);
        User
            .create({
                username: "ngyh",
                password: hashpassword,
                admin: false,
                iqdat: true,
                testtime: false
            })
            .then(function(user) {
                console.log(user);
            })
            .catch(function(err) {
                console.log(err);
            });
        
        var hashpassword = bcrypt.hashSync("gundamm0bile", bcrypt.genSaltSync(8), null);
        User
            .create({
                username: "nugraha",
                password: hashpassword,
                admin: false,
                iqdat: false,
                testtime: true
            })
            .then(function(user) {
                console.log(user);
            })
            .catch(function(err) {
                console.log(err);
            });
    }
}