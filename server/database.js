var Sequelize = require('sequelize');
var config = require("./config");

var database = new Sequelize(config.mysql, {
    pool: {
        max: 4,
        min: 1,
        idle: 10000
    }
});

var UserList = require("./model/user.list.model.js")(database);
var PasswordList = require("./model/password.list.model.js")(database);

// MySQL relationship
UserList.belongsTo(PasswordList, {foreignKey: 'username', targetKey: 'username'});

database
    .sync({force: config.seed})
    .then(function () {
        console.log("Database in Sync Now");
        require("./seed")();
    });

module.exports = {
    UserList: UserList,
    PasswordList: PasswordList
};

