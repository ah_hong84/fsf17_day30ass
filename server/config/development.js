module.exports = {
    mysql: "mysql://root:hong@localhost/user?reconnect=true",
    
    domain_name: "http://localhost:3000",
    aws: {
        id: "xxxx",
        key: "xxxx",
        url: "xxxx",
        bucket: "xxxx",
        region: "ap-southeast-1"
    },
    
    port: 3000,
    seed: false
};
