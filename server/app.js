// Load the libraries
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var request = require('request');

// Congifure the port
const NODE_PORT = process.env.NODE_PORT || 3030;

// TODO check the dirname is correct when test .
const CLIENT_FOLDER = path.join(__dirname, '../client');

// Create an instance of the app
var app = express();

// Setup of the configuration of express.
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));






//Endpoint --> Search starting
app.post("/api/search/:part_no", function (req, resp) {

    var postData = { //inventory search in SAP SJP 
        "query": "SELECT DISTINCT " + //query SJP SG01
        "AL1.MATERIAL_ID, AL1.PLANT_ID, AL1.STORAGE_LOC, AL1.UNRESTRICTED_STK, AL1.TRANSFER_STK, AL1.QUALITY_STK, " +
        "AL1.RESTRICT_USE_STK, AL1.BLOCK_STK, AL1.RETURN_STK, AL2.PURCHASE_GROUP, AL2.PROC_TYPE " +
        "FROM ODSSI.MATERIAL_SLOC AL1 LEFT OUTER JOIN ODSSI.MATERIAL_V AL2 " +
        "ON AL1.MATERIAL_ID=AL2.MATERIAL_ID AND AL1.PLANT_ID=AL2.PLANT_ID " +
        "WHERE (AL1.PLANT_ID='SG01' AND AL1.MATERIAL_ID LIKE ('%" + req.params.part_no + "%')) " +

        "UNION ALL " +

        "SELECT DISTINCT " + //query SJP SJ01
        "AL1.MATERIAL_ID, AL1.PLANT_ID, AL1.STORAGE_LOC, AL1.UNRESTRICTED_STK, AL1.TRANSFER_STK, AL1.QUALITY_STK, " +
        "AL1.RESTRICT_USE_STK, AL1.BLOCK_STK, AL1.RETURN_STK, AL2.PURCHASE_GROUP, AL2.PROC_TYPE " +
        "FROM ODSSI.MATERIAL_SLOC AL1 LEFT OUTER JOIN ODSSI.MATERIAL_V AL2 " +
        "ON AL1.MATERIAL_ID=AL2.MATERIAL_ID AND AL1.PLANT_ID=AL2.PLANT_ID " +
        "WHERE (AL1.PLANT_ID='SJ01' AND AL1.MATERIAL_ID LIKE ('%" + req.params.part_no + "%')) " +

        "UNION ALL " +

        "SELECT DISTINCT " + //query SJP GD01
        "AL1.MATERIAL_ID, AL1.PLANT_ID, AL1.STORAGE_LOC, AL1.UNRESTRICTED_STK, AL1.TRANSFER_STK, AL1.QUALITY_STK, " +
        "AL1.RESTRICT_USE_STK, AL1.BLOCK_STK, AL1.RETURN_STK, AL2.PURCHASE_GROUP, AL2.PROC_TYPE " +
        "FROM ODSSI.MATERIAL_SLOC AL1 LEFT OUTER JOIN ODSSI.MATERIAL_V AL2 " +
        "ON AL1.MATERIAL_ID=AL2.MATERIAL_ID AND AL1.PLANT_ID=AL2.PLANT_ID " +
        "WHERE (AL1.PLANT_ID='GD01' AND AL1.MATERIAL_ID LIKE ('%" + req.params.part_no + "%')) " +

        "UNION ALL " +

        "SELECT DISTINCT " + //query SJP HG11
        "AL1.MATERIAL_ID, AL1.PLANT_ID, AL1.STORAGE_LOC, AL1.UNRESTRICTED_STK, AL1.TRANSFER_STK, AL1.QUALITY_STK, " +
        "AL1.RESTRICT_USE_STK, AL1.BLOCK_STK, AL1.RETURN_STK, AL2.PURCHASE_GROUP, AL2.PROC_TYPE " +
        "FROM ODSSI.MATERIAL_SLOC AL1 LEFT OUTER JOIN ODSSI.MATERIAL_V AL2 " +
        "ON AL1.MATERIAL_ID=AL2.MATERIAL_ID AND AL1.PLANT_ID=AL2.PLANT_ID " +
        "WHERE (AL1.PLANT_ID='HG11' AND AL1.MATERIAL_ID LIKE ('%" + req.params.part_no + "%')) " +

        "UNION ALL " +

        "SELECT DISTINCT " + //query RP0 1232
        "AL1.MATERIAL_ID, AL1.PLANT_ID, AL1.STORAGE_LOC, AL1.UNRESTRICTED_STK, AL1.TRANSFER_STK, AL1.QUALITY_STK, " +
        "AL1.RESTRICT_USE_STK, AL1.BLOCK_STK, AL1.RETURN_STK, AL2.PURCHASE_GROUP, AL2.PROC_TYPE " +
        "FROM ODS.MATERIAL_SLOC AL1 LEFT OUTER JOIN ODS.MATERIAL_V AL2 " +
        "ON AL1.MATERIAL_ID=AL2.MATERIAL_ID AND AL1.PLANT_ID=AL2.PLANT_ID " +
        "WHERE (AL1.PLANT_ID='1232' AND AL1.MATERIAL_ID LIKE ('%" + req.params.part_no + "%')) " +

        "UNION ALL " +

        "SELECT DISTINCT " + //query RP0 1202
        "AL1.MATERIAL_ID, AL1.PLANT_ID, AL1.STORAGE_LOC, AL1.UNRESTRICTED_STK, AL1.TRANSFER_STK, AL1.QUALITY_STK, " +
        "AL1.RESTRICT_USE_STK, AL1.BLOCK_STK, AL1.RETURN_STK, AL2.PURCHASE_GROUP, AL2.PROC_TYPE " +
        "FROM ODS.MATERIAL_SLOC AL1 LEFT OUTER JOIN ODS.MATERIAL_V AL2 " +
        "ON AL1.MATERIAL_ID=AL2.MATERIAL_ID AND AL1.PLANT_ID=AL2.PLANT_ID " +
        "WHERE (AL1.PLANT_ID='1202' AND AL1.MATERIAL_ID LIKE ('%" + req.params.part_no + "%')) "
    }

    var url = 'http://localhost:3000/api/db2/r0adb2'
    console.log("token: " + req.body.token);
    var options = {
        url: url,
        method: 'POST',
        body: postData,
        headers: {
            Authorization: "JWT " + req.body.token,
            'Content-Type': 'application/json'
        }
    }
    request(options, function (err, res, body) {
        if (err) {
            console.error('error posting json: ', err)
            throw err
        }
        var headers = res.headers
        var statusCode = res.statusCode
        console.log('headers: ', headers)
        console.log('statusCode: ', statusCode)
        console.log('body: ', body)
        resp.status(200).json(body);
    })
});




// Start the server
app.listen(NODE_PORT, function () {
    console.log("Server is running at port " + NODE_PORT);
})