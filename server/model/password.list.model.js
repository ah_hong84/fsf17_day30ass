var Sequelize = require('sequelize');

module.exports = function(database) {
    return database.define('user', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        isAdmin: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        reset_password_token: {
            type: Sequelize.STRING,
            allowNull: true
        },
        reset_password_sent_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
        db_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        updated_on: {
            type: Sequelize.DATE,
            allowNull: false
        },
        next_update: {
            type: Sequelize.DATE,
            allowNull: false
        }
    })
}