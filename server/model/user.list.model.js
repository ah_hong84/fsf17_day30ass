var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('user_information', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true
        },
        username: {
            type : Sequelize.STRING,
            allowNull: false
            primaryKey: true,
        },
        firstName: {
            type : Sequelize.STRING,
            allowNull: false
        },
        lastName: {
            type : Sequelize.STRING,
            allowNull: false
        },
        email: {
            type:Sequelize.STRING,
            allowNull: false
        },
        created_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    });
};