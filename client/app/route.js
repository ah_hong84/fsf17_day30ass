(function () {
    angular
        .module("QDashApp")
        .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function uirouterAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            // --------- Route for /admin/login ---------
            .state("Login", {
                url: "/Login",
                templateUrl: "app/login/login.html",
                controller: "LoginCtrl",
                controllerAs: "ctrl"
            })
            // --------- All routes for /protected/admin ---------
            .state("Admin", {
                url: "/Admin",
                templateUrl: "app/protected/admin/admin.html",
                // controller: "AdminCtrl",
                // controllerAs: "ctrl"
            })
            .state("Password-List", {
                url: "/Password-List",
                templateUrl: "app/protected/admin/password.list.html",
                // controller: "PasswordListCtrl",
                // controllerAs: "ctrl"
            })
            .state("Password-Add", {
                url: "/Password-Add",
                parent: "Password-List",
                templateUrl: "app/protected/admin/password.add.html",
                // controller: "PasswordAddCtrl",
                // controllerAs: "ctrl"
            })
            .state("Password-Edit", {
                url: "/Password-Edit",
                parent: "Password-List",
                templateUrl: "app/protected/admin/password.edit.html",
                // controller: "PasswordEditCtrl",
                // controllerAs: "ctrl"
            })
            .state("User-List", {
                url: "/User-List",
                templateUrl: "app/protected/admin/user.list.html",
                // controller: "UserListCtrl",
                // controllerAs: "ctrl"
            })
            .state("User-Add", {
                url: "/User-Add",
                parent: "User-List",
                templateUrl: "app/protected/admin/user.add.html",
                // controller: "UserAddCtrl",
                // controllerAs: "ctrl"
            })
            .state("User-Edit", {
                url: "/User-Edit",
                parent: "User-List",
                templateUrl: "app/protected/admin/User.edit.html",
                // controller: "UserEditCtrl",
                // controllerAs: "ctrl"
            })

            // --------- Route for /protected/inventory ---------
            .state("Inventory", {
                url: "/Inventory",
                templateUrl: "app/protected/inventory/inventory.html",
                controller: "InventoryCtrl",
                controllerAs: "ctrl"
            })

            // --------- All routes for /protected/orders ---------
            .state("Orders", {
                url: "/Orders",
                templateUrl: "app/protected/orders/orders.html",
                //controller: "OrdersCtrl",
                //controllerAs: "ctrl"
            })
            .state("Orders-Detail", {
                url: "/Orders-Detail",
                parent: "Orders",
                templateUrl: "app/protected/orders/orders.detail.html",
                //controller: "OrdersDetailCtrl",
                //controllerAs: "ctrl"
            })
            .state("Orders-Summary", {
                url: "/Orders-Summary",
                parent: "Orders",
                templateUrl: "app/protected/orders/orders.summary.html",
                //controller: "OrderSummaryCtrl",
                //controllerAs: "ctrl"
            })

            // --------- Route for /protected/search ---------
            .state("Search", {
                url: "/Search",
                templateUrl: "app/protected/search/search.html",
                controller: "SearchCtrl",
                controllerAs: "ctrl"
            })

            // --------- Route for /protected/summary ---------
            .state("Summary", {
                url: "/Summary",
                templateUrl: "app/protected/summary/summary.html",
                controller: "SummaryCtrl",
                controllerAs: "ctrl"
            })

            // --------- Route for /protected/whereused ---------
            .state("Where-Used", {
                url: "/Where-Used",
                templateUrl: "app/protected/whereused/whereused.html",
                controller: "WhereUsedCtrl",
                controllerAs: "ctrl"
            })

        $urlRouterProvider.otherwise("/Home");

    }

})();