//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function () {
    //create an instance of my Angular app
    angular
        .module("QDashApp")
        .service("QDashService", QDashService);

    QDashService.$inject = ["$http", "$state"];
    var token = "";

    //define the function to be used as service
    function QDashService($http, $state) {
        var service = this;

        //expose, so that it can be used by other modules
        service.login = login;
        service.search = search;
        
        //Login function
        function login(username, password) {
            //$state.go("Search"); // redirect to search page
            $http.post('http://localhost:3000/login', {username: username, password: password})
                .then(function(res) {
                    console.log(res);
                    token = res.data.token;
                    $state.go("Search"); // redirect to search page
                });
        }

        //Search function
        function search(part_no) {
            //var params = JSON.stringify({token : token});
            console.log("in service");

            return $http.post("/api/search/" + part_no, {token: token})
                .then(function(res){
                    return res;
                });
        }

    }
})();