//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){
    //create an instance of my Angular app
    angular
        .module("QDashApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ['QDashService']; //inject the dependency

    //define the function to be used as controller
    function SearchCtrl(QDashService){
        var vm = this;

        vm.search = search; //expose "search", so that it can be used by other modules
        
        function search(){
            console.log("in controller");
            QDashService.search(vm.part_no)   //input is using vm.part_no
                 .then(function(result){
                     console.log("Result " + JSON.stringify(result));
                 }).catch(function(err){
                     console.log("error " + JSON.stringify(err));
                 })
        }
    }    
})();