//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){
    //create an instance of my Angular app
    angular
        .module("QDashApp")
        .controller("LoginCtrl", LoginCtrl);

    LoginCtrl.$inject = ['QDashService']; //inject the dependency

    //define the function to be used as controller
    function LoginCtrl(QDashService){
        var vm = this;
        vm.login = login;

        vm.username = "";
        vm.password = "";
        
        function login(){
            console.log("in login controller");
            QDashService.login(vm.username, vm.password);   //input is using vm.username and vm.password
                //  .then(function(result){
                //      console.log("Result " + JSON.stringify(result));
                //  }).catch(function(err){
                //      console.log("error " + JSON.stringify(err));
                //  })
        }
    }    
})();